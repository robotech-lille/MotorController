/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define ENCODER_3B_Pin GPIO_PIN_13
#define ENCODER_3B_GPIO_Port GPIOC
#define ENCODER_4A_Pin GPIO_PIN_14
#define ENCODER_4A_GPIO_Port GPIOC
#define ENCODER_4B_Pin GPIO_PIN_15
#define ENCODER_4B_GPIO_Port GPIOC
#define ENCODER_5A_Pin GPIO_PIN_0
#define ENCODER_5A_GPIO_Port GPIOC
#define ENCODER_5B_Pin GPIO_PIN_1
#define ENCODER_5B_GPIO_Port GPIOC
#define ENCODER_0A_Pin GPIO_PIN_6
#define ENCODER_0A_GPIO_Port GPIOA
#define ENCODER_0B_Pin GPIO_PIN_7
#define ENCODER_0B_GPIO_Port GPIOA
#define MOTOR_0A_Pin GPIO_PIN_0
#define MOTOR_0A_GPIO_Port GPIOB
#define MOTOR_0B_Pin GPIO_PIN_1
#define MOTOR_0B_GPIO_Port GPIOB
#define LED_Pin GPIO_PIN_2
#define LED_GPIO_Port GPIOB
#define MOTOR_1A_Pin GPIO_PIN_10
#define MOTOR_1A_GPIO_Port GPIOB
#define MOTOR_1B_Pin GPIO_PIN_11
#define MOTOR_1B_GPIO_Port GPIOB
#define EMERGENCY_Pin GPIO_PIN_12
#define EMERGENCY_GPIO_Port GPIOB
#define MOTOR_2A_Pin GPIO_PIN_15
#define MOTOR_2A_GPIO_Port GPIOB
#define MOTOR_2B_Pin GPIO_PIN_6
#define MOTOR_2B_GPIO_Port GPIOC
#define MOTOR_3A_Pin GPIO_PIN_7
#define MOTOR_3A_GPIO_Port GPIOC
#define ENCODER_1A_Pin GPIO_PIN_8
#define ENCODER_1A_GPIO_Port GPIOC
#define MOTOR_3B_Pin GPIO_PIN_9
#define MOTOR_3B_GPIO_Port GPIOC
#define MOTOR_4A_Pin GPIO_PIN_8
#define MOTOR_4A_GPIO_Port GPIOA
#define MOTOR_4B_Pin GPIO_PIN_9
#define MOTOR_4B_GPIO_Port GPIOA
#define MOTOR_5A_Pin GPIO_PIN_10
#define MOTOR_5A_GPIO_Port GPIOA
#define ENCODER_1B_Pin GPIO_PIN_12
#define ENCODER_1B_GPIO_Port GPIOC
#define ENCODER_2A_Pin GPIO_PIN_2
#define ENCODER_2A_GPIO_Port GPIOD
#define MOTOR_5B_Pin GPIO_PIN_3
#define MOTOR_5B_GPIO_Port GPIOB
#define ENCODER_2B_Pin GPIO_PIN_4
#define ENCODER_2B_GPIO_Port GPIOB
#define ENCODER_3A_Pin GPIO_PIN_5
#define ENCODER_3A_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
