#include <erreur.h>
#include <struct.h>

#define NB_MOTOR 6

//declartion de la liste des moteurs
struct motor l_motor[NB_MOTOR];


void set_pid(struct motor * l_motor)
{
	for(int i = 0; i < NB_MOTOR; i++)
	{
		l_motor.motor[i].pid->kP = 1;
		l_motor.motor[i].pid->kI = 0;
		l_motor.motor[i].pid->kD = 0;
		l_motor.motor[i].pid->hacheur = 1;
		l_motor.motor[i].pid->lastError = 0;
		l_motor.motor[i].pid->integral = 0;
	}
}

struct motor choose_motor(char * motor, struct motor * l_motor)
{
/*
	while(l_motor != NULL)
	{
		if(strcmp(motor,"motor_0") == 0)
			return motor_0;
		if(strcmp(motor,"motor_1") == 0)
			return motor_1;
		if(strcmp(motor,"motor_2") == 0)
			return motor_2;
		if(strcmp(motor,"motor_3") == 0)
			return motor_3;
		if(strcmp(motor,"motor_4") == 0)
			return motor_4;
		if(strcmp(motor,"motor_5") == 0)
			return motor_5;
		l_motor = l_motor->suivant;
	}
*/
	return l_motor.motor[atoi(motor[6])];
}


int iterate_function(char * action, struct motor motor, int value)//motor à trouver avec choose_motor
{
	int time_step = 1;

	if(strcmp(action,"set_kp") == 0)
	{
		motor.pid.kp = value;
		return ITERATE_FUNCTION_OK;;
	}
	if (strcmp(action,"set_ki") == 0)
	{
		motor.pid.ki = value;
		return ITERATE_FUNCTION_OK;
	}
	if (strcmp(action,"set_kd") == 0)
	{
		motor.pid.kd = value;
		return ITERATE_FUNCTION_OK;
	}
	if (strcmp(action,"set_hacheur") == 0)
	{
		motor.pid.hacheur = value;
		return ITERATE_FUNCTION_OK;
	}
	if (strcmp(action,"set_consigne_regulation"))
	{
		motor.consigne_regulation = value;
		return ITERATE_FUNCTION_OK;
	}
	return ITERATE_FUNCTION_ACTION_ERROR;
}

/*
	//regulation courant
	if(strcmp(out_payload, regulation_courant_motor_0a) == 0)
	{
		motor->consigne = pid_step(&pid_mA0, time_set, motor->consigne - motor->mesure);
//		on regule en courant;
//		on stocke l info;
		return ITERATE_FUNCTION_OK;
	}

	//regulation position
	elsif(strcmp(out_payload, regulation_position) == 0)
	{
//		on regule en position;
//		on stocke l info;
		return ITERATE_FUNCTION_OK;

	}

	//regulation vitesse
	elsif(strcmp(out_payload, regulation_vitesse) == 0);
	{
//		on regule en vitesse;
//		on stocke l info;
		return ITERATE_FUNCTION_OK;

	}
	elsif(strcmp(out_payload, "speed0") == 0)
		{
	//		on regule en vitesse;
	//		on stocke l info;
			speed_motor_0 = atoi(out_value);
			return ITERATE_FUNCTION_OK;

		}

	else
		return ITERATE_FUNCTION_PAYLOAD_ERROR;//mauvais out_payload;
		*/
}

