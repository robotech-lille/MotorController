/*
 * struct.h
 *
 *  Created on: Nov 18, 2022
 *      Author: romai
 */

struct pid
{
    // Controller gains
    int kP;
    int kI;
    int kD;
    int hacheur;
    // State variables
    int lastError;
    int integral;
};

struct motor
{
	int consigne_moteur;
	int consigne_regulation;
	struct pid pid;
	int mesure_courant;
	int mesure_position;
	int mesure_vitesse;
	int mode;//0=courant 1=position 2=vitesse
};
