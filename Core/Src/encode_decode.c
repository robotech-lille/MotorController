#include <erreur.h>

void encode(char * payload, char * value, char ** output, int size_max) //
{
	snprintf(output, size_max, "{%s:%s ", payload, value);
	unsigned int current_size = 1 + strlen(payload) + 1 + strlen(value) + 1;
	char CRC=0;
	for(int i = 0; i < current_size; i++)
	{
		CRC += output[i];
	}
	snprintf(*(output[current_size]), size_max - current_size, "%2X}", CRC);
}

int decode(char * message, char ** out_action, char ** out_motor, int * out_value, int size_max)
{
	unsigned int message_size = strlen(message);//taille du message

	//verification CRC
	char CRC_control = 0;
	for(int i = 0; i < message_size - 3; i++)
	{
		CRC_control += message[i];
	}
	char CRC;
	int scan_value = sscanf(message, "{%s_%s:%s %x}", *out_action, *out_motor, out_value, &CRC);
	if (scan_value != 3) return DECODE_CRC_INVALID_SSCANF_ERROR;//probleme avec le sscanf
	if (CRC_control != CRC)
		return DECODE_CRC_ERROR;//si CRC pas bon

	//verification synthaxe message de type {variable:valeur CRC}
	if(message[i]!='{') return DECODE_INVALID_MESSAGE_ERROR; //ne commence pas par '{'
	int i=0;
	while((i < size_max - 5) and (message[i] != ':'))//
	{
		i++;
	}
	if (message[i] != ':') return DECODE_INVALID_MESSAGE_ERROR; //il n'y a pas ':'
	while((i < size_max - 3) and (message[i] != ' '))
	{
		i++;
	}
	if (message[i] != ' ') return DECODE_INVALID_MESSAGE_ERROR; //il n'y a pas ' '
	if (message_size > size_max) return DECODE_INVALID_MESSAGE_ERROR; //message trop long
	if (message[message_size]) != '}') return DECODE_INVALID_MESSAGE_ERROR;//ne fini pas par '}'


	//verification bon moteur
	if(strcmp(*out_motor, "motor_0") != 0)
		if(strcmp(*out_motor, "motor_1") != 0)
			if(strcmp(*out_motor, "motor_2") != 0)
				if(strcmp(*out_motor, "motor_3") != 0)
					if(strcmp(*out_motor, "motor_4") != 0)
						if(strcmp(*out_motor, "motor_5") != 0)
							return DECODE_INVLID_MOTOR_ERROR;

	return DECODE_OK;// si ça a marché/pas d'erreur
}
