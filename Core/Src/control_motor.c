/*
 * control_motor.c
 *
 *  Created on: Nov 18, 2022
 *      Author: romai
 */

#include <struct.h>

int pid_step(struct pid *controller, int dt, int error)
{
    // Calculate p term
    int p = error * controller->kP;

    // Calculate i term
    controller->integral += error * dt * controller->kI;

    // Calculate d term, taking care to not divide by zero
    int d =
        dt == 0 ? 0 : ((error - controller->lastError) / dt) * controller->kD;
    controller->lastError = error;

    return (p + controller->integral + d) * controller->hacheur;
}

void regulation_motor(struct motor * motor)//pour l'instant que regulation en courant
{
	int dt = 1;
	if (mode == 0)
		motor->consigne_moteur = pid_step(&motor, dt, motor->consigne_regulation - motor->mesure_courant) * motor->pid.hacheur;
	if (mode == 1)
		motor->consigne_moteur = pid_step(&motor, dt, motor->consigne_regulation - motor->mesure_position) * motor->pid.hacheur;
	if (mode == 2)
		motor->consigne_moteur = pid_step(&motor, dt, motor->consigne_regulation - motor->mesure_vitesse) * motor->pid.hacheur;
}
